import math

pi = math.pi

def detectLetter(points):
    #keypoints = [nose, leftEye, rightEye, leftEar, rightEar, 
    #     5: lShoul rShoul, leftElbor rightElbow leftWriest rightWrist leftHiip rightHip
    #     13:leftKnee rightKnee leftAnkle rightAnkle]
    
    shoulderAngle = angle(lShoul, rShoul) # + = tilted right on camera, - = tilted left on camera
    # left shoulder to left wrist. straight out = 0
    LArmAngle = angle(lShoul, lWrist)
    # right shoulder to right wrist. straight out = pi
    RArmAngle = angle(rShoul, rWrist)
    # hip angle, straight = 0
    hipAngle = angle(lHip, rHip)
    # left shoulder to left wrist. doing a split = 0
    LLegAngle = angle(lHip, lKnee)
    # right shoulder to right wrist. doing a split = pi
    RLegAngle = angle(rHip, rKnee)
    '''
     ------
     |     |
     |     |
     _a___b|

    torso lean is measured as a+b. standing still should be zero (since a = 90 and b = -90), so if a increases and b decreases

'''
    a = angle(lHip, lShoul) - angle(lHip, rHip)
    b = angle(rHip, lHip) - angle(rHip, rShoul)
    torsoAngleEstimated = a+b

    #distance, in pixels, between hands
    handDistance = distance(lWrist, lWrist)
    
    #find closest letter
    poseVec = [shoulderAngle, LArmAngle, RArmAngle, hipAngle, torsoAngleEstimated,LLegAngle,RLegAngle]
    print(poseVec)

    letters = {
      # shoulderAngle, lArm, rArm, hip, torso lean, lLeg, rLeg
        A:[0,         pi/2,    pi/2,     0,    0,        -3*pi/4,    -pi/4],
        T:[0,            0,    0,    0,    0,        -pi/2,    -pi/2],
        Y:[0,         3*pi/4,    3*pi/4, 0,    0,        -pi/2,    -pi/2],
        X:[0,         3*pi/4,    3*pi/4, 0,    0,        -3*pi/4,    -pi/4],
    }
    
    weights =[
           1,             1,        1,        1,    1,            0,0]


    #now attempt to find the closest letter
    smallestDist = 20
    foundLetter = None
    for i in letters:
        newDist = dist(letters[i], poseVec, weights)
        if(newDist < smallestDist):
            smallestDist = newDist
            foundLetter =i
        
    

    if(smallestDist == 20):
        #no letter found
        return
    
    #have foundLetter


def angleDiff(a1,a2):
    pi2 = 2*pi
    return (a1+a2) % pi2


def angle(p1,p2):
    return math.atan2(p1.y - p2.y,p1.x - p2.x)


def distFromLetterVec(p1,p2):
    #manhattan distance, not euclidean dist. todo: change
    if(p1.length != p2.length):
        raise ValueError("vectors not same length")
    
    j=0
    for i in range(p1):
        thisCoordDiff = abs(angleDiff(p1[i],p2[i]))
        if(thisCoordDiff > pi/2): #if too far away, penalize
            thisCoordDiff += 5
        
        j += thisCoordDiff * weights[i]
    
    return j



#unused
def dist(p1,p2, weights):
    #manhattan distance, not euclidean dist. todo: change
    if(p1.length != p2.length):
        raise ValueError("vectors not same length")
    
    j=0
    for i in range(p1):
        j += abs(angleDiff(p1[i],p2[i]))*weights[i]
    
    return j


def distance(p1,p2):
    #in pixel coords
    return Math.sqrt((p1.y - p2.y)*(p1.y - p2.y)+(p1.x - p2.x)*(p1.x - p2.x))

