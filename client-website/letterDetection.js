const pi = Math.PI;

let lastPts = null;
let threshold = 10;
let timeout = null;
const CONFIRM_TIME = 500;

function detectLetters(poses, minPoseConfidence, minPartConfidence) {
  poses.forEach(({score, keypoints}) => {
    if (score < minPoseConfidence) {
      return; //will move onto next pose because this is a function
    }
    lastPts = keypoints;

    /*keypoints = [nose, leftEye, rightEye, leftEar, rightEar,
       5: lShoul rShoul, leftElbor rightElbow leftWriest rightWrist leftHiip rightHip
        13:leftKnee rightKnee leftAnkle rightAnkle]
        x.position */

    let lShoul = keypoints[5].position, rShoul = keypoints[6].position, lWrist = keypoints[9].position,
      rWrist = keypoints[10].position;
    let lHip = keypoints[11].position, rHip = keypoints[12].position, lKnee = keypoints[13].position,
      rKnee = keypoints[14].position, lElbow = keypoints[7].position, rElbow = keypoints[8].position;


    //Now, set variables and angles of various limbs


    let shoulderAngle = angle(lShoul, rShoul); // + = tilted right on camera, - = tilted left on camera
    // left shoulder to left wrist. straight out = 0
    let LArmAngle = angle(lShoul, lWrist);


    // 0 = unbent and stretched, pi = full bend. pi/2 = clockwise 90 degrees   -
    //                                                                          |
    let LElbowBendAmt = 0;
    if (lElbow !== undefined) {
      LElbowBendAmt = angleRound(-(angle(lElbow, lShoul) - angle(lWrist, lElbow)));
    }

    // right shoulder to right wrist. straight out = pi
    let RArmAngle = angle(rShoul, rWrist);

    // 0 = unbent and stretched, pi = full bend. pi/2 = clockwise 90 degrees |
    //                                                                       --

    let RElbowBendAmt = 0;
    if (rElbow !== undefined) {
      RElbowBendAmt = pi - (angle(rElbow, rShoul) - angle(rElbow, rWrist));
    }

    let LHBelow = lWrist.y > lShoul.y;
    let RHBelow = rWrist.y > rShoul.y;
    console.log(LHBelow, RHBelow);

    // hip angle, straight = 0
    let hipAngle = angle(lHip, rHip);

    // left shoulder to left wrist. doing a split = 0
    let LLegAngle = angle(lHip, lKnee);

    // right shoulder to right wrist. doing a split = pi
    let RLegAngle = angle(rHip, rKnee);

    /*
       ------
       |     |
       |     |
       _a___b|

        torso lean is measured as a+b. standing still should be zero (since a = 90 and b = -90), so if a increases and b decreases

        */
    let a = angle(lHip, lShoul) - angle(lHip, rHip);
    let b = angle(rHip, lHip) - angle(rHip, rShoul);
    let torsoAngleEstimated = a + b;


    //distance, in pixels, between hands
    let handDistance = distance(lWrist, lWrist);

    document.getElementById('shoulderAngle').innerText = shoulderAngle.toFixed(1);
    document.getElementById('lArm').innerText = LArmAngle.toFixed(1);
    document.getElementById('rArm').innerText = RArmAngle.toFixed(1);
    document.getElementById('hipAngle').innerText = hipAngle.toFixed(1);
    document.getElementById('torsoAngle').innerText = torsoAngleEstimated.toFixed(1);
    document.getElementById('lLeg').innerText = LLegAngle.toFixed(1);
    document.getElementById('rLeg').innerText = RLegAngle.toFixed(1);
    document.getElementById('lElbow').innerText = LElbowBendAmt.toFixed(1);
    document.getElementById('rElbow').innerText = RElbowBendAmt.toFixed(1);


    //find closest letter
    let poseVec =
      [shoulderAngle, LArmAngle, RArmAngle, hipAngle, torsoAngleEstimated, LLegAngle, RLegAngle, LElbowBendAmt, RElbowBendAmt];

    //arms over head at 45 degrees: 3pi/4, pi/4
    //arms straight out: pi, 0
    //arm


    /*



    A: o
      /|\
      /\

       _
      |
    C: o
       |\/
      /\


    /*
      / \
    M:\o/
       |
      /\


    */

    let letters = {
      // shoulderAngle, lArm, rArm, hip, torso lean,   lLeg,    rLeg, LElbow Relbow
      A: [0, -3 * pi / 4, -pi / 4, 0, pi, -pi / 2, -pi / 2, 0, 0],
      C: [0.7, -pi / 2, 0, 0, pi, -pi / 2, -pi / 2, -pi / 2, pi / 2],
      M: [0, pi / 2, pi / 2, 0, pi, -pi / 2, -pi / 2, -pi / 2, pi / 2],
      S: [0, -pi / 2, -pi / 2, 0, pi, -pi / 2, -pi / 2, pi / 2, -pi / 2],
      T: [0, pi, 0, 0, pi, -pi / 2, -pi / 2, 0, 0],
      W: [0, 3 * pi / 4, pi / 4, 0, pi, -pi / 2, -pi / 2, -pi / 2, pi / 2], //like Y but bent
      X: [0, 3 * pi / 4, pi / 4, 0, pi, -pi / 2 - 0.5, -pi / 2 + 0.5, 0, 0],
      Y: [0, 3 * pi / 4, pi / 4, 0, pi, -pi / 2, -pi / 2, 0, 0],
      Z: [0, pi / 2, pi / 2, 0, pi, -pi / 2, -pi / 2, -pi / 2, pi / 2],
    }

    let hands = {
      //  Lhand, rhand above/below their shoulder joint?
      A: ['b', 'b'],
      C: ['a', '-'],
      M: ['a', 'a'],
      T: ['-', '-'],
      W: ['a', 'a'],
      X: ['a', 'a'],
      Y: ['a', 'a'],
      Z: ['a', 'b'],
      S: ['b', 'a'],
    }


    let useLeg = 0;
    if (keypoints[13].score > minPartConfidence && keypoints[14].score > minPartConfidence) {
      useLeg = 1;//0 to disable legs
      document.getElementById('leg').innerText = '/';
    } else {
      document.getElementById('leg').innerText = 'x';
    }

    let weights =
      [1, 1, 1, 1, 0, useLeg, useLeg, 2, 2];


    let smallestDist = threshold;
    let foundLetter = null;
    for (var i in letters) {

      if (i in hands) {
        //left hand where it's supposed to be?
        if (((hands[i][0] == 'a') && LHBelow) || ((hands[i][0] == 'b') && !LHBelow)) {
          continue;
        }
        //right hand where it's supposed to be?
        if (((hands[i][1] == 'a') && RHBelow) || ((hands[i][1] == 'b') && !RHBelow)) {
          continue;
        }
      }
      let newDist = dist(letters[i], poseVec, weights);
      if (newDist < smallestDist) {
        smallestDist = newDist;
        foundLetter = i;
      }
    }

    if (smallestDist == threshold) {
      //no letter found
      document.getElementById('letter').innerText = '-';
      document.getElementById('dist').innerText = '';
      return
    }

    const letterDisplay = document.getElementById('letter');

    // send command to press key once user holds pose for half a second
    if (letterDisplay.innerText !== foundLetter) {
      console.log(`cancelling registration of "${letterDisplay.innerText}"`);
      clearTimeout(timeout);
      timeout = null;
    }
    if (timeout === null) {
      console.log(`setting a timeout for "${foundLetter}"`);
      timeout = setTimeout(function () {
        fetch(`/press?key=${foundLetter}`).then(result =>
          console.log(`timeout complete, "${foundLetter}": ${result}`));
        a = null;
      }, CONFIRM_TIME);
    }

    letterDisplay.innerText = foundLetter;
    document.getElementById('dist').innerText = smallestDist.toFixed(1);

    let names = ['tshoulderAngle', 'tlArm', 'trArm', 'thipAngle', 'ttorsoAngle', 'tlLeg', 'trLeg', 'tlElbow', 'trElbow'];
    for (var i = 0; i < names.length; i++) {
      document.getElementById(names[i]).innerText = angleRound(letters[foundLetter][i]).toFixed(1);
    }


  });
}

function angleDiff(a1, a2) {
  return Math.PI - Math.abs(Math.abs(a1 - a2) - Math.PI);
}

function angleRound(x) {
  let pi2 = 2 * Math.PI;
  return ((x % pi2) + pi2) % pi2;
}

function angle(p1, p2) {
  return angleRound(Math.atan2(p1.y - p2.y, p1.x - p2.x));
}

function distFromLetterVec(p1, p2, weights) {
  //euclidean dist.
  if (p1.length != p2.length) {
    throw new Error("vectors not same length");
  }
  let j = 0;
  for (var i = 0; i < p1.length; i++) {
    let thisCoordDiffSq = angleDiff(p1[i], p2[i]) * angleDiff(p1[i], p2[i]);
    if (thisCoordDiffSq > Math.PI / 2) {
      thisCoordDiff += 5; //penalize
    }
    j += thisCoordDiff * weights[i];
  }
  return Math.sqrt(j);
}


//unused
function dist(p1, p2, weights) {
  //manhattan distance, not euclidean dist. todo: change
  if (p1.length != p2.length) {
    throw new Error("vectors not same length");
  }
  let j = 0;
  for (var i = 0; i < p1.length; i++) {
    j += Math.abs(angleDiff(p1[i], p2[i])) * weights[i];
  }
  return j;
}

function distance(p1, p2) {
  //in pixel coords
  return Math.sqrt((p1.y - p2.y) * (p1.y - p2.y) + (p1.x - p2.x) * (p1.x - p2.x));
}

function todeg(x) {
  return x / Math.PI * 180 + 'deg';
}
