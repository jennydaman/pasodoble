#!/usr/bin/env python3

from flask import Flask, request, send_from_directory, jsonify
import os
import pyautogui

app = Flask(__name__, static_url_path='', static_folder='client-website')

@app.route("/")
def root():
    return send_from_directory('client-website', 'index.html')

@app.route('/press')
def key():
    qs = request.args
    if 'key' not in qs:
        return jsonify(False), 400
    pyautogui.press(qs['key'])
    return jsonify(True)

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=int(os.environ.get('PORT', 5000)))
