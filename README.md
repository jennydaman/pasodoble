# pasodoble

For the 2019 [Boston Stupid Shit No One Needs & Terrible Ideas Hackathon](https://bostonstupidhackathon.com/).

`index.html` is a virtual keyboard which inputs keystrokes that you pose and hold
for half a second.

First, install python dependencies `flask` and `pyautogui`.

```bash
python3 server.py
```

Project and based on [`PoseNet`](https://github.com/tensorflow/tfjs-models/tree/master/posenet).
Uses a pre-trained neural network to classify body posture, and joint angles to determine the letter.

This project is far from complete, we hope it is never completed.

## Sceenshots

![The letter 'T'](screenshots/jennings%20t.png)
![The letter 'X'](screenshots/jennings%20x.png)
![The letter 'Z'](screenshots/jennings%20z.png)

## Notes

`pyautogui` does not work well on GNOME v3.32.1 on Wayland.
Special buttons (e.g. `capslock`) do not register.
Letters will only appear in text fields (e.g. text editor, URL bar).

